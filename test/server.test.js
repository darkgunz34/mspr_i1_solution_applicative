const createServer = require("../server")
const supertest = require("supertest");
const randomMail = require('crypto');
const { Server } = require("https");
const { timeout } = require("nodemon/lib/config");

const MAIL_FOR_TEST = `${randomMail.randomBytes(10).toString('hex')}@test.nodejs`;


describe('Our application', function() {
jest.setTimeout(10000);
const app = createServer.creerUnServeur();

test("POST /creation du compte Success", async () => {
	const data = {
		Name: "steven",
		Email: MAIL_FOR_TEST
	};
	await supertest(app)
		.post("/create")
		.send(data)
		.expect(200)
});
test("POST /creation du compte Doublon", async () => {
	const data = {
		Name: "steven",
		Email: "mon-adresse@mail.com"
	};
	await supertest(app)
		.post("/create")
		.send(data)
		.expect(501)
});

test("POST /creation du compte name manquant", async () => {
	const data = {
		Email: MAIL_FOR_TEST
	};
	await supertest(app)
		.post("/create")
		.send(data)
		.expect(401)
});

test("POST /creation du compte email manquant", async () => {
	const data = {
		name: "steven"
	};
	await supertest(app)
		.post("/create")
		.send(data)
		.expect(401)
});

});


