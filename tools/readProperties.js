var propertiesReader = require('properties-reader');

//The ressources.projet is located to gestion-sous-cron ONLY. Don't move the file
var pathOfProperties = './resources.properties';
var properties = propertiesReader(pathOfProperties);


function getValue(key){
    return properties.get(`mspr.${key}`);
}

function setValueFromProjectName(nameOfProject,key,value){
    console.log("ecriture");
    properties.set(`${nameOfProject}.${key}`,value);
    properties.save(pathOfProperties);
}

module.exports = {
    getValue
};
