var express = require('express');
var https = require('https');
const fs = require('fs');
const routes = require('./route');


function creerUnServeur() {
    const app = express();

    https.createServer({
        key: fs.readFileSync('server.key'),
        cert: fs.readFileSync('server.cert')
      }, app)
      .listen(8080, function () {
        console.log('Express Server is running');
      });
    app.use("/",routes);
    return app;
}

exports.creerUnServeur = creerUnServeur;
