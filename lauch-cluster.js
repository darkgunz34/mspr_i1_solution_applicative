//https://www.youtube.com/watch?v=Iz0MFOKmmDY
var cluster = require('cluster');
var https = require('https');
const createServer = require("./server");

var numCPUs = 4;

if (cluster.isMaster) {
    console.log(`start server with ${process.argv[2]}`);
    for (var i = 0; i < numCPUs; i++) {
        console.log("initialisation d'un CPU");
        cluster.fork();
    }

    cluster.on("online", worker => {
      console.log(
        `worker with id:${worker.id} & pid:${worker.process.pid} is online`
      )
    })  
} else {
  createServer.creerUnServeur();
}


