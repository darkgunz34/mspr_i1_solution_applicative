const express = require("express");
const router = express.Router();
const jsforce = require('jsforce');
const parser = require('body-parser');
const properties = require("./tools/readProperties");
const url = properties.getValue("url");
const username = properties.getValue("username");
const password = properties.getValue("password")
const token = properties.getValue("token")

router.use(parser.urlencoded({
    extended: true
}));
router.use(parser.json());

const checkIfProd = process.argv[2] || "dev";

// url de connexion salesforce
const conn = new jsforce.Connection({
    loginUrl: url
});

//Test si l'application est bien lancer
router.get('/', function(req, res) {
    res.status(200);
    res.send('server start');
});

// creation compte utilisateur Salesforce (+ connexion préalable)
router.post('/create', async function(req, res) {
    try {
        await conn.login(username, password + token, (err, userInfo) => {
            if(err) {
                res.status(501);
            }
        });
        const name = req.body.Name;
        const email = req.body.Email;

        if(!name || !email) {
            res.status(401);
        } else{
            if(checkIfProd == "prod") {
                await conn.sobject("Account").create({ Name : name, Email__c : email}, function(err, ret) {
                    if (err || !ret.success) {
                        res.status(501);
                    }
                    else{
                        res.status(200);
                    }
                });
            }else{
                //UNIQUEMENT POUR LA PHASE DE TEST
                if(email == "mon-adresse@mail.com"){
                    res.status(501);
                }else{
                    res.status(200);
                }
            }    
        }
    }catch(err) {
        console.log(err);
        res.status(501);
    }
    res.send('');
}); 


module.exports = router;
