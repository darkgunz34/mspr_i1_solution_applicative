# mspr_i1_solution_applicative

## POC pour le bloc A5C2

Titre : Concevoir une architecture applicative selon la complexité du système d’information existant de type architecture distribuée, ou micro-service évolutive et tolérante aux pannes.

Le candidat/La candidate est en capacité de proposer une architecture applicative :

- Il/Elle argumente son choix en formalisant par écrit les critères de stabilité, d’efficacité et de pérennité.
- Il/Elle liste l’environnement technique adéquat à son architecture applicative (architecture distribuée, clusters, architecture micro-services, REST, appel des services…)
- Il/Elle présente sous forme de schéma la logique du fonctionnement de son architecture applicative.

### Solution proposer

Mise en place d'une API en mode cluster afin de faire du load balancing permettant la répartition de charge.

Dans le cadre du poc, la solution retenu est celle contenu dans le fichier `lauch-cluster.js`. On détermine un nombre d'instance processeur à lancer en // (ici au nombre de 4).

Cela revient à faire une application multi-thread permettant d'offrir un temps de réponse plus rapide en fonction du nombre d'utilisateur.

#### Installation / Lancement

0. Clonner le repo : `git clone https://gitlab.com/darkgunz34/mspr_i1_solution_applicative.git`
1. Installer les nodes modules : `nmp -i`.
2. Lancer le projet : `node lauch-cluster` | en mode débug : `npm run dev`.
3. Pour la démo cluster :
    1. Télécharger la lib node (en global) suivante : `npm i loadtest -g`.
    2. Ouvrir 3 ou 4 cmd / bash et tapper la commande suivante : `loadtest -n 10000 http://localhost:8000/`.
    3. Cette commande permettrat de simuler du trafic réseau (a hauteur de 10 000 requête par prompt) => venir exploiter plusieurs processus.
    4. Regarder dans la console du projet et plusieur PID devraient ressortir.

Pour plus d'information :

- [Lien Avantage Cluster](https://apachebooster.com/blog/what-is-server-clustering-and-how-does-it-work/#:~:text=Server%20clusters%20are%20constructed%20in%20such%20a%20way,depend%20on%20IP%20based%20network%20technologies%20to%20function)
- [tuto en node](https://www.youtube.com/watch?v=Iz0MFOKmmDY)
- [Node cluster](https://nodejs.org/api/cluster.html)

## Processus d'authentification

Pour se connecter et débrancher sur l'appel de l'ERP, il faut passer par l'adresse suivante : `https://localhost/authentification?user=myName&mail=mymail@gmail.com`.

## Mise en place de TU

Lancer la commande `npm test` Equivalent à cette commande : `jest --forceExit`. Moyen de forcer l'arrêt du serveur quand l'ensemble des tests sont passés.

Lancer la commande `npm start` Equivalent à cette commande : `node lauch-cluster.js`.

## Certificats SSL

Ici, les certificats sont autosigné, ce ne sont donc pas ceux à prendre pour la production (les autres sont en place sur le serveur) mais à ce servir pour la phase de dev UNIQUEMENT.
